package bipkg

import "github.com/Limard/windowsapi"

func init() {
	Is64OS = windowsapi.Is64bitOS()

	LineFuncMap["wow64"] = wow64
}

var wow64Values []uintptr

func wow64(t *ScriptLine) (res string, e error) {
	if t.ArgsN >= 1 && t.Args[0] == "disable" {
		wow64Values = append(wow64Values, Wow64DisableWow64FsRedirection())
	} else {
		if len(wow64Values) > 0 {
			Wow64RevertWow64FsRedirection(wow64Values[len(wow64Values)-1])
			wow64Values = wow64Values[:len(wow64Values)-1]
		}
	}
	return
}

func getSysPathVariables() map[string]string {
	var localConstValue = make(map[string]string, 12)

	ov := Wow64DisableWow64FsRedirection()
	defer Wow64RevertWow64FsRedirection(ov)

	localConstValue["CommonDesktopDir"], _ = windowsapi.GetCommonDesktopDir()

	localConstValue["PrinterDriverDirX86"], _ = windowsapi.GetPrinterDriverDirectory86()
	localConstValue["PrinterDriverDirX64"], _ = windowsapi.GetPrinterDriverDirectory64()

	localConstValue["PrinterProcessorDirX86"], _ = windowsapi.GetPrintProcessorDirectory86()
	localConstValue["PrinterProcessorDirX64"], _ = windowsapi.GetPrintProcessorDirectory64()

	localConstValue["WindowsDir"], _ = windowsapi.GetWindowsDir()

	localConstValue["ProgramFilesDir"], _ = windowsapi.GetProgramFilesDir()
	localConstValue["ProgramFiles86Dir"], _ = windowsapi.GetProgramFiles86Dir()

	localConstValue["CommonAppDataDir"], _ = windowsapi.GetCommmonAppDataDirectory()

	localConstValue["SystemDir"], _ = windowsapi.GetSystemDir()
	localConstValue["SystemDir86"], _ = windowsapi.GetSystem86Dir()

	localConstValue["TempDir"], _ = windowsapi.GetTempPath()
	localConstValue[`[StartMenuDir]`], _ = windowsapi.SHGetSpecialFolderPath(windowsapi.CSIDL_COMMON_STARTMENU)

	localConstValue["Local"] = windowsapi.GetAppExePath()

	localConstValue["CommonStartUp"], _ = windowsapi.SHGetSpecialFolderPath(windowsapi.CSIDL_COMMON_STARTUP)
	localConstValue["StartUp"], _ = windowsapi.SHGetSpecialFolderPath(windowsapi.CSIDL_STARTUP)

	return localConstValue
}

func Wow64DisableWow64FsRedirection() uintptr {
	if Is64OS == false {
		return 0
	}

	r, e := windowsapi.Wow64DisableWow64FsRedirection()
	if e != nil {
		Logs.Debug(e)
	}
	return r
}

func Wow64RevertWow64FsRedirection(oldvalue uintptr) {
	if Is64OS == false {
		return
	}

	e := windowsapi.Wow64RevertWow64FsRedirection(oldvalue)
	if e != nil {
		Logs.Debug(e)
	}
}
