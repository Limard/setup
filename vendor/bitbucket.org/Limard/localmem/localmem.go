package localmem

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"fmt"
)

func init() {
}

// Path is localmem数据存储路径
var Path string

func code(str []byte) (buf []byte) {
	// log.Printf("str len: %#+v\n", len(str))
	key := "{3841BBF4-4212-43EE-A420-A0C043489BFD}"
	keylen := len(key)
	strlen := len(str)
	buf = make([]byte, strlen)
	for index := 0; index < strlen; index++ {
		buf[index] = str[index] ^ key[index%keylen]

	}
	// log.Printf("buf len: %#+v\n", len(buf))
	return buf
}

// Set is set json to localmemcache
// Deprecated
func Set(keyName string, value interface{}) error {
	buf, err := json.Marshal(value)
	if err != nil {
		// log.Println("err:", err.Error())
		return err
	}
	// log.Printf("json(write): %#+v\n", string(buf))

	path := getLocalMemPath()
	filePath := path + "/" + keyName

	f, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		// log.Println("err:", err.Error())
		return err
	}
	defer f.Close()

	// code
	buf = code(buf)

	f.Write(buf)
	return nil
}

// SetValue2 ...
// Deprecated
func SetValue2(keyName string, value interface{}, cipher bool) error {
	return SetValueJson(keyName, value, cipher)
}

// SetValueJson ...
func SetValueJson(keyName string, value interface{}, cipher bool) error {
	buf, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return SetValueBuf(keyName, buf, cipher)
}

// SetValueBuf ...
func SetValueBuf(keyName string, buf []byte, cipher bool) error {
	path := getLocalMemPath()
	filePath := path + "/" + keyName

	if cipher {
		buf = code(buf)
	}

	return ioutil.WriteFile(filePath, buf, 0666)
}

// Get is get string from LocalMemcache
// Deprecated
func Get(keyname string) ([]byte, error) {
	filePath := getLocalMemPath() + "/" + keyname
	if _, err := os.Stat(filePath); err != nil && os.IsNotExist(err) == true {
		return []byte(""), errors.New("This keyname is invaild.")
	}

	buf, err := ioutil.ReadFile(filePath)
	if err != nil {
		return []byte(""), errors.New("Cannot read this keyname.")
	}

	// log.Printf("buflen: %#+v\n", len(buf))
	// str = strings.Trim(string(buf), string(0))

	// code
	switch buf[0] {
	case 0, 32: // encode {, [
		buf = code(buf)
	case 123, 91: // {, [
		// nothing
	}
	return buf, nil
}

// GetValue ...
// Deprecated
func GetValue(keyName string, value interface{}) (err error) {
	filePath := getLocalMemPath() + "/" + keyName
	if _, err := os.Stat(filePath); err != nil && os.IsNotExist(err) == true {
		return errors.New("this keyName is invalid")
	}

	buf, err := ioutil.ReadFile(filePath)
	if err != nil {
		return errors.New("cannot read this keyName")
	}

	// code
	switch buf[0] {
	case 0, 32: // encode {, [
		buf = code(buf)
	case 123, 91: // {, [
		// nothing
	}

	err = json.Unmarshal(buf, value)
	if err != nil {
		return err
	}
	// log.Println(i)

	return nil
}

// GetValue2 is get object from localmem
func GetValue2(keyName string, value interface{}) (cipher bool, err error) {
	filePath := getLocalMemPath() + "/" + keyName
	if _, err := os.Stat(filePath); err != nil && os.IsNotExist(err) == true {
		return false, errors.New("this keyName is invalid")
	}

	buf, err := ioutil.ReadFile(filePath)
	if err != nil {
		return false, errors.New("cannot read this keyName")
	}

	if len(buf) == 0 {
		return false, nil
	}

	if value == nil {
		return false, nil
	}

	switch buf[0] {
	case 123, 91: // {, [
		err = json.Unmarshal(buf, value)
		return false, err

	case 0, 32: // encode {, [
		buf = code(buf)
		err = json.Unmarshal(buf, value)
		return true, err
	}
	return false, fmt.Errorf("cannot parse value. %v", err)
}

// Enum to get all localmemcache name
func Enum() (names []string) {
	mempath := getLocalMemPath()
	filepath.Walk(mempath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		names = append(names, info.Name())
		return nil
	})
	return names
}

// Remove localmem key
func Remove(keyName string) (e error) {
	filePath := getLocalMemPath() + "/" + keyName
	return os.Remove(filePath)
}
