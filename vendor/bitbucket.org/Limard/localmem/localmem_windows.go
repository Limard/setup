package localmem

import (
	"os"
	"syscall"
	"unsafe"
)

func getLocalMemPath() string {
	if Path != "" {
		return Path
	}

	appData, _ := getCommmonAppDataDirectory()
	Path = appData + `\PrintSystem\LocalMem`
	if _, err := os.Stat(Path); err != nil && os.IsNotExist(err) == true {
		os.MkdirAll(Path, 0666)
	}
	return Path
}

var (
	dshell32                = syscall.NewLazyDLL("Shell32.dll")
	pSHGetSpecialFolderPath = dshell32.NewProc("SHGetSpecialFolderPathW")
	cCSIDL_COMMON_APPDATA   = 0x23
)

func getCommmonAppDataDirectory() (string, error) {
	return shGetSpecialFolderPath(cCSIDL_COMMON_APPDATA)
}

func shGetSpecialFolderPath(nFolder int) (string, error) {
	if err := pSHGetSpecialFolderPath.Find(); err != nil {
		return "", err
	}
	pt := make([]uint16, syscall.MAX_PATH)
	ret, _, err := pSHGetSpecialFolderPath.Call(
		uintptr(0),
		uintptr(unsafe.Pointer(&pt[0])),
		uintptr(nFolder),
		uintptr(1))
	if ret != 0 {
		err = nil
	}

	return syscall.UTF16ToString(pt), err
}
