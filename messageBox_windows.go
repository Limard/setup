package bipkg

import (
	"github.com/Limard/windowsapi"
	"os"
)

// true(ok), false(cancel)
func messageBoxOKCancel(text, caption string) bool {
	ret, e := windowsapi.MessageBox(text, caption, windowsapi.MB_OKCANCEL)
	if e != nil {
		Logs.Error(e)
		os.Exit(1)
	}
	if ret == windowsapi.IdCancel {
		return false
	}
	return true
}

func messageBoxYesNo(text, caption string) bool {
	ret, e := windowsapi.MessageBox(text, caption, windowsapi.MB_YESNO)
	if e != nil {
		Logs.Error(e)
		os.Exit(1)
	}
	if ret == windowsapi.IdNo {
		return false
	}
	return true
}

func messageBoxOK(text, caption string) {
	windowsapi.MessageBox(text, caption, windowsapi.MB_OK)
}
