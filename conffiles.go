package bipkg

import "strings"

//[conffiles]节点
//一行一个文件
//在安装时不覆盖指定文件
//卸载时默认不卸载指定文件

type ConfFilesSection []string

func (t *ConfFilesSection) parseVariable(env map[string]string) {
	for i := range *t {
		for key, value := range env {
			(*t)[i] = strings.Replace((*t)[i], "["+key+"]", value, 1)
		}
	}
}

func (t *ConfFilesSection) IsConfFile(p string) bool {
	for _, s := range *t {
		if s == p {
			return true
		}
	}
	return false
}