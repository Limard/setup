package bipkg

import (
	"archive/zip"
	"bytes"
	"io"
	"os"
	"path/filepath"
)

func unzipReader(f *os.File, unzipPath string) (e error) {
	fi, _ := f.Stat()
	r, e := zip.NewReader(f, fi.Size())
	if e != nil {
		return
	}

	unzip(r.File, unzipPath)
	return
}

func unzipFile(zipFile, unzipPath string) {
	r, err := zip.OpenReader(zipFile)
	if err != nil {
		Logs.Error(err, zipFile)
		return
	}
	defer r.Close()

	unzip(r.File, unzipPath)
}

func unzipBuf(buffer []byte, unzipPath string) {
	read := bytes.NewReader(buffer)
	r, err := zip.NewReader(read, int64(len(buffer)))
	if err != nil {
		Logs.Error(err)
		return
	}

	unzip(r.File, unzipPath)
}

func unzip(file []*zip.File, unzipPath string) {
	for _, k := range file {
		/////// Dir
		if k.FileInfo().IsDir() {
			dir := filepath.Join(unzipPath, k.Name)
			Logs.Infof("MkDir: %v \r\n", dir)
			err := os.MkdirAll(dir, 0644)
			if err != nil {
				Logs.Error(err)
			}
			continue
		}

		/////// File
		rc, err := k.Open()
		if err != nil {
			Logs.Error(err)
			continue
		}
		defer rc.Close()

		unzipFilePath := filepath.Join(unzipPath, k.Name)
		unzipFileDir := filepath.Dir(unzipFilePath)
		os.MkdirAll(unzipFileDir, 0666)

		NewFile, err := os.Create(unzipFilePath)
		if err != nil {
			Logs.Error(err)
			continue
		}
		written, err := io.Copy(NewFile, rc)
		Logs.Debugf("unzipFile: %s, written: %v", unzipFilePath, written)
		if err != nil {
			Logs.Error(err)
		}
		NewFile.Close()
	}
}
