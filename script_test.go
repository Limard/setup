package bipkg

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
)

func TestGetScript(t *testing.T) {
	f, e := os.Open(`F:\Installer\2005 20版打刻windows终端\setup.script`)
	if e != nil {
		t.Fatal(e)
	}
	defer f.Close()

	s, e := GetScript(f, nil, &Args{
		Slice:                   false,
		IgnoreExecutionError:    false,
		IgnoreInstalledChecking: false,
	})
	if e != nil {
		t.Fatal(e)
	}

	//b, e := json.MarshalIndent(s, "", "  ")
	b, e := json.Marshal(s)
	if e != nil {
		t.Fatal(e)
	}
	t.Log(string(b))

	for _, ss := range s.UninstallSection.Scripts {
		fmt.Println(ss.Keyword, ss.Args)
	}
}