copy /B /Y ./bipkg/main.go ./bipkg_asUser/main.go

set GOOS=linux
set GOARCH=amd64
go build -ldflags "-w -s" -o ./bin/%GOOS%_%GOARCH%/bipkg ./bipkg_asUser

set GOARCH=mips64le
go build -ldflags "-w -s" -o ./bin/%GOOS%_%GOARCH%/bipkg ./bipkg_asUser

set GOARCH=arm64
go build -ldflags "-w -s" -o ./bin/%GOOS%_%GOARCH%/bipkg ./bipkg_asUser


if errorlevel 1 pause
