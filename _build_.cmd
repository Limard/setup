@echo off

SET GOOS=windows
SET GOARCH=386

rem 1.10 support Windows XP
set GOROOT=C:\Go1.10.8
set PATH=C:\Go1.10.8\bin;%PATH%

rem go build -ldflags="-H windowsgui" -o bipkg.exe
go build -v -ldflags "-s -w" -o .\bin\bipkg.exe .\bipkg
go build -v -ldflags "-s -w" -o ./bin/%GOOS%_%GOARCH%/bipkg.exe ./bipkg_asUser

if errorlevel 1 pause
