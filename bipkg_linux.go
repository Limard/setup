package bipkg

import (
	"os"
	"path/filepath"
)

func renewBipkg() error {
	savePath := filepath.Join(PkgRecord.WorkDir, "bipkg")
	currentPath, _ := os.Readlink(`/proc/self/exe`)

	saveInfo, e := os.Stat(savePath)
	if e != nil && os.IsNotExist(e) {
		e = CopyFile(currentPath, savePath)
		if e != nil {
			return e
		}
		os.Chmod(savePath, 0777)
		return os.Symlink(savePath, `/usr/bin/bipkg`)
	}

	currentInfo, e := os.Stat(currentPath)
	if e != nil {
		return e
	}

	if currentInfo.ModTime().After(saveInfo.ModTime()) {
		e = CopyFile(currentPath, savePath)
		if e != nil {
			return e
		}
		os.Chmod(savePath, 0777)
		return os.Symlink(savePath, `/usr/bin/bipkg`)
	}
	return nil
}