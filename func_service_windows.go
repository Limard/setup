package bipkg

import (
	"fmt"
)

func serviceUninstallClear(name string) error {
	return DeleteReg(fmt.Sprintf(`HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\eventlog\Application\%v`, name))
}
