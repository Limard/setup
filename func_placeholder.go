package bipkg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

// 修正json配置文件中的变量

func init() {
	LineFuncInfo["placeholderjson"] = "placeholderjson desPath srcPath"
}

func placeholderJson(t *ScriptLine, env map[string]string) (res string, e error) {
	if t.ArgsN == 2 {
		buf, e := ioutil.ReadFile(t.Args[1])
		if e != nil {
			return "", e
		}

		var v interface{}
		e = json.Unmarshal(buf, &v)
		if e != nil {
			return "", e
		}
		v = placeholderJsonObject(v, env)

		buf, e = json.MarshalIndent(v, "", "  ")
		if e != nil {
			return "", e
		}

		e = ioutil.WriteFile(t.Args[0], buf, 0666)
		if e != nil {
			return "", e
		}
	}
	return
}

func placeholderJsonArray(vv []interface{}, env map[string]string) []interface{} {
	var res []interface{}
	for i, u := range vv {
		switch vv1 := u.(type) {
		case string:
			for s, s2 := range env {
				vv1 = strings.Replace(vv1, s, s2, -1)
			}
			res = append(res, vv1)
		case float64:
			res = append(res, vv1)
		case bool:
			res = append(res, vv1)
		case nil:
			res = append(res, vv1)
		case []interface{}:
			res = append(res, placeholderJsonArray(vv1, env))
		case interface{}:
			res = append(res, placeholderJsonObject(vv1, env))
		default:
			fmt.Println("  ", i, "[type?_]", u, ", ", vv1)
			res = append(res, vv1)
		}
	}
	return res
}

func placeholderJsonObject(f interface{}, env map[string]string) interface{} {
	m := f.(map[string]interface{})
	for k, v := range m {
		switch vv := v.(type) {
		case string:
			for s, s2 := range env {
				vv = strings.Replace(vv, s, s2, -1)
			}
			m[k] = vv
		case float64:
		case bool:
		case nil:
		case []interface{}:
			m[k] = placeholderJsonArray(vv, env)
		case interface{}:
			m[k] = placeholderJsonObject(vv, env)
		default:
			fmt.Println(k, "[type?]", vv)
		}
	}
	return m
}
