package bipkg

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/Limard/windowsapi"
	"golang.org/x/sys/windows/registry"
)

func init() {
	// [vc100x86] call xxxxx.zip
	LineFuncMap["depend"] = depend
}

func depend(t *ScriptLine) (res string, e error) {
	return "", ParseDepends(t.Args).Check()
}

// name [version ><=1.2.23.2] [call xxxx]
func ParseDepends(args []string) *depends {
	d := new(depends)
	for i := 0; i < len(args); i++ {
		if i == 0 {
			d.name = args[i]
			continue
		}

		switch args[i] {
		case "version":
			d.version = args[i+1]
		case "call":
			d.call = args[i+1]
		case "url":
			d.url = args[i+1]
		}
		i++
	}
	return d
}

type depends struct {
	name    string
	version string
	call    string
	url     string
}

func (t *depends) Check() error {
	if len(t.name) == 0 {
		return fmt.Errorf("depend format error")
	}

	if t.checkDependsVCName() {
		Logs.Infof("depend %s, check ok", t.name)
		return nil
	}
	if t.checkDependPkgName() {

	}

	// 尝试安装
	if t.goCall() == nil {
		return nil
	}
	if t.goUrl() == nil {
		return nil
	}
	return fmt.Errorf("缺少依赖组件 %v", t.name)
}

func (t *depends) checkDependsVCName() bool {
	ok := false
	var clsid []string
	switch t.name {
	case "[vc80x86]":
		clsid := []string{"{A49F249F-0C91-497F-86DF-B2585E8E76B7}", "{7299052b-02a4-4627-81f2-1818da5d550d}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkWinsxsDirectory(`x86_microsoft.vc80.crt`)
		}

	case "[vc80x64]":
		clsid := []string{"{6E8E85E8-CE4B-4FF5-91F7-04999C9FAE6A}", "{071c9b48-7c32-4621-a0ac-3f809523288f}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkWinsxsDirectory(`amd64_microsoft.vc80.crt`)
		}

	case "[vc90x86]":
		clsid = []string{"{FF66E9F6-83E7-3A3E-AF14-8DE9A809A6A4}", "{9A25302D-30C0-39D9-BD6F-21E6EC160475}",
			"{86CE1746-9EFF-3C9C-8755-81EA8903AC34}", "{1F1C2DFC-2D24-3E06-BCB8-725134ADF989}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkWinsxsDirectory(`x86_microsoft.vc90.crt`)
		}

	case "[vc90x64]":
		clsid = []string{"{350AA351-21FA-3270-8B7A-835434E766AD}", "{8220EEFE-38CD-377E-8595-13398D740ACE}",
			"{5FCE6D76-F5DC-37AB-B2B8-22AB8CEDB1D4}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkWinsxsDirectory(`amd64_microsoft.vc90.crt`)
		}

	case "[vc100x86]":
		clsid = []string{"{196BB40D-1578-3D01-B289-BEFC77A11A1E}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkSystemX86([]string{"msvcr100.dll", "msvcp100.dll"})
		}

	case "[vc100x64]":
		clsid = []string{"{DA5E371C-6333-3D8A-93A4-6FD5B20BCC6E}"}
		ok = checkWindowsUninstall(clsid)
		if ok == false {
			ok = checkSystemX64([]string{"msvcr100.dll", "msvcp100.dll"})
		}
	}
	//Logs.Infof("%v", ok)
	return ok
}

func (t *depends) checkDependPkgName() bool {
	for _, pkg := range PkgRecord.ListPkg() {
		if pkg.Name == t.name {
			if len(t.version) == 0 {
				return true
			}
			return t.checkVersion(pkg.Version)
		}
	}
	return false
}

func (t *depends) checkVersion(installedVersion string) bool {
	const gt = 1 << 2
	const lt = 1 << 1
	const eq = 1 << 0

	r := 0
	requestVersion := ""
top:
	for i := 0; i < len(t.version); i++ {
		switch t.version[i] {
		case '>':
			r |= gt
		case '<':
			r |= lt
		case '=':
			r |= eq
		default:
			requestVersion = t.version[i:]
			break top
		}
	}
	if r == 0 {
		r |= gt | eq
	}

	c, e := VersionCompare(requestVersion, installedVersion)
	if e != nil {
		Logs.Error(e)
		return false
	}
	//fmt.Println(requestVersion, installedVersion, c, r, r&(1<<2))
	if c > 0 && r&gt != 0 {
		return true
	} else if c < 0 && r&lt != 0 {
		return true
	} else if c == 0 && r&eq != 0 {
		return true
	}
	return false
}

func (t *depends) goCall() error {
	//if len(t.call) != 0 {
	//	Logs.Infof("depend %s try to call %v", t.name, t.call)
	//	return CallScriptFile(t.call, nil, sectionNameInstall)
	//}
	return fmt.Errorf("no call script")
}

func (t *depends) goUrl() error {
	return fmt.Errorf("no url script")
}

func checkWindowsUninstall(guid []string) bool {
	if windowsapi.Is64bitOS() {
		for _, g := range guid {
			key := fmt.Sprintf(`SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\%v`, g)
			k, e := registry.OpenKey(registry.LOCAL_MACHINE, key, registry.READ|registry.WOW64_64KEY)
			if e == nil {
				k.Close()
				return true
			}
		}
	}

	for _, g := range guid {
		key := fmt.Sprintf(`SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\%v`, g)
		k, e := registry.OpenKey(registry.LOCAL_MACHINE, key, registry.READ|registry.WOW64_32KEY)
		if e == nil {
			k.Close()
			return true
		}
	}
	return false
}

func checkWinsxsDirectory(name string) bool {
	winsxs, e := windowsapi.GetWindowsDir()
	if e != nil {
		Logs.Error(e)
		return false
	}
	winsxs = filepath.Join(winsxs, `winsxs`)
	e = filepath.Walk(winsxs, func(fPath string, fInfo os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if fInfo.IsDir() == false {
			return nil
		}
		n := fInfo.Name()
		if strings.Contains(n, name) {
			Logs.Infof("find %v", n)
			return fmt.Errorf("ok")
		}
		return nil
	})
	if e != nil && e.Error() == "ok" {
		return true
	}
	return false
}

func checkSystemX86(filename []string) bool {
	dir, e := windowsapi.GetSystem86Dir()
	if e != nil {
		return false
	}
	for _, f := range filename {
		_, e := os.Stat(filepath.Join(dir, f))
		if e == nil {
			return true
		}
	}
	return false
}

func checkSystemX64(filename []string) bool {
	v, e := windowsapi.Wow64DisableWow64FsRedirection()
	if e == nil {
		defer windowsapi.Wow64RevertWow64FsRedirection(v)
	}

	dir, e := windowsapi.GetSystemDir()
	if e != nil {
		return false
	}
	for _, f := range filename {
		_, e := os.Stat(filepath.Join(dir, f))
		if e == nil {
			return true
		}
	}
	return false
}
