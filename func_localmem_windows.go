package bipkg

import (
	"bitbucket.org/Limard/localmem"
	"encoding/json"
	"reflect"
)

func init() {
	// name filepath [cipher(false)]
	LineFuncMap["localmemimport"] = localmemimport
	LineFuncInfo[`localmemimport`] = `localmemimport name filepath [cipher]`

	// name
	LineFuncMap["localmemremove"] = localmemremove
	LineFuncInfo[`localmemremove`] = `localmemremove name`

	// name filepath [cipher(false)]
	LineFuncMap["localmemmerge"] = localmemmerge
	LineFuncInfo[`localmemmerge`] = `localmemmerge name filepath [cipher]`

	// name
	LineFuncMap["readlocalmem"] = readlocalmem
	LineFuncInfo[`readlocalmem`] = `localmemmerge name filepath [cipher]`
}

func localmemimport(t *ScriptLine) (res string, e error) {
	cipher := false
	if t.ArgsN >= 3 && t.Args[2] == "true" {
		cipher = true
	}
	return "", LocalMemImport(t.Args[0], t.Args[1], cipher)
}
func localmemremove(t *ScriptLine) (res string, e error) {
		return "", LocalMemRemove(t.arg(0))
}
func localmemmerge(t *ScriptLine) (res string, e error) {
	cipher := false
	if t.ArgsN >= 3 && t.Args[2] == "true" {
		cipher = true
	}
	return "", LocalMemMerge(t.Args[0], t.Args[1], cipher)
}
func readlocalmem(t *ScriptLine) (res string, e error) {
	return LocalMemRead(t.arg(0))
}

func LocalMemImport(name, filePath string, cipher bool) error {
	buf, e := GetFileBuffer(filePath)
	if e != nil {
		return e
	}

	var value interface{}
	if e = json.Unmarshal(buf, &value); e != nil {
		return e
	}

	return localmem.SetValueJson(name, value, cipher)
}

func LocalMemRemove(name string) error {
	return localmem.Remove(name)
}

func LocalMemMerge(name, filePath string, cipher bool) error {
	buf, e := GetFileBuffer(filePath)
	if e != nil {
		return e
	}

	var value interface{}
	if e = json.Unmarshal(buf, &value); e != nil {
		return e
	}

	var oldValue interface{}
	localmem.GetValue2(name, &oldValue)
	value = jsonMerge(oldValue, value)

	return localmem.SetValueJson(name, value, cipher)
}

func LocalMemRead(name string) (value string, e error) {
	b, e := localmem.Get(name)
	if e != nil {
		return "", e
	}
	return string(b), nil
}

func jsonMerge(dst, src interface{}) interface{} {
	srcMap, srcMapOk := jsMapify(dst)
	dstMap, dstMapOk := jsMapify(src)
	if srcMapOk && dstMapOk {
		return jsMerge(dstMap, srcMap, 0)
	}
	return dst
}

func jsMerge(dst, src map[string]interface{}, depth int) map[string]interface{} {
	var jsonMergeDepth = 32
	if depth > jsonMergeDepth {
		return dst
	}

	for key, srcVal := range src {
		if dstVal, ok := dst[key]; ok {
			srcMap, srcMapOk := jsMapify(srcVal)
			dstMap, dstMapOk := jsMapify(dstVal)
			if srcMapOk && dstMapOk {
				srcVal = jsMerge(dstMap, srcMap, depth+1)
			}
		}

		dst[key] = srcVal
	}

	return dst
}

func jsMapify(i interface{}) (map[string]interface{}, bool) {
	value := reflect.ValueOf(i)
	if value.Kind() == reflect.Map {
		m := map[string]interface{}{}
		for _, k := range value.MapKeys() {
			m[k.String()] = value.MapIndex(k).Interface()
		}
		return m, true
	}
	return map[string]interface{}{}, false
}
