//+build none

package bipkg

import (
	"gopkg.in/ini.v1"
	"os"
	"path/filepath"
	"strconv"
)

func init() {
	// name version
	//LineFuncMap["moduleinfo"] = moduleinfo
	//LineFuncMap["setmoduleinfo"] = moduleinfo
	//LineFuncMap["writemoduleinfo"] = moduleinfo

	// name
	//LineFuncMap["delmoduleinfo"] = moduleinfo
	//LineFuncMap["deletemoduleinfo"] = delmoduleinfo
}

func moduleinfo(t *ScriptLine) (res string, e error) {
	if t.ArgsN >= 2 {
		return "", SetModuleInfo(t.Args[0], t.Args[1])
	}
	return
}
func delmoduleinfo(t *ScriptLine) (res string, e error) {
	if t.ArgsN >= 1 {
		return "", DelModuleInfo(t.Args[0])
	}
	return
}

func SetModuleInfo(moduleName, moduleVersoin string) error {
	fileName := filepath.Join(constValue["[CommonAppDataDir]"], `PrintSystem`, `ModuleInfo.lst`)
	f, e := ini.Load(fileName)
	if e != nil {
		if os.IsNotExist(e) == false {
			return e
		}

		f = ini.Empty()
		s, _ := f.NewSection("ModuleInfo")
		s.NewKey("ModuleCount", "0")
	}

	section := f.Section("ModuleInfo")
	kModuleCount := section.Key("ModuleCount")
	count, _ := kModuleCount.Int()

	for i := 0; i < count; i++ {
		section1 := f.Section("Module" + strconv.Itoa(i+1))
		name, e := section1.GetKey("ModuleName")
		if e != nil {
			continue
		}
		if name.String() == moduleName {
			version, _ := section1.GetKey("ModuleVersion")
			version.SetValue(moduleVersoin)
			return f.SaveTo(fileName)
		}
	}

	// add new
	count++
	ns, e := f.NewSection("Module" + strconv.Itoa(count))
	if section == nil {
		return e
	}
	ns.NewKey("ModuleName", moduleName)
	ns.NewKey("ModuleVersion", moduleVersoin)

	kModuleCount.SetValue(strconv.Itoa(count))

	return f.SaveTo(fileName)
}

func DelModuleInfo(moduleName string) error {
	fileName := filepath.Join(constValue["[CommonAppDataDir]"], `PrintSystem`, `ModuleInfo.lst`)
	f, e := ini.Load(fileName)
	if e != nil {
		return e
	}

	s := f.Section("ModuleInfo")
	kCount := s.Key("ModuleCount")
	count, _ := kCount.Int()

	d := false
	for i := 0; i < count; i++ {
		section1 := f.Section("Module" + strconv.Itoa(i+1))
		name, e := section1.GetKey("ModuleName")
		if e != nil {
			continue
		}
		if name.String() == moduleName {
			d = true
			count--
		}
		if d {
			section2 := f.Section("Module" + strconv.Itoa(i+2))
			name2 := section2.Key("ModuleName").String()
			ver2 := section2.Key("ModuleVersion").String()
			section1.Key("ModuleName").SetValue(name2)
			section1.Key("ModuleVersion").SetValue(ver2)
		}
	}

	if d {
		f.DeleteSection("Module" + strconv.Itoa(count+1))
		kCount.SetValue(strconv.Itoa(count))
	}

	return f.SaveTo(fileName)
}
