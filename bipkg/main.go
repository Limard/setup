package main

import (
	"bitbucket.org/Limard/bipkg"
	"bitbucket.org/Limard/logx"
	"flag"
	"fmt"
	"os"
	"runtime/debug"
	"strings"
)

type bipkgArgs struct {
	Slice   bool
	Verbose bool
	Install bool
	Remove  bool
	Purge   bool
	List    bool
	Force   bool
	Json    bool
}

func printUsage() {
	fmt.Println("App usage:")
	flag.PrintDefaults()

	fmt.Println("")
	fmt.Println("Script command:")
	bipkg.PrintFunc()

	fmt.Println("")
	fmt.Println("Script options:")
	fmt.Println("\t-x86/-x64:")
	fmt.Println("\t-f: \t no stop when an error occurs")
}

func main() {
	defer func() {
		err := recover()
		if err != nil {
			logx.Errorf(`Crash: %v`, err)
			logx.Errorf(`Stack: %s`, string(debug.Stack()))
		}
	}()

	var args bipkgArgs
	flag.BoolVar(&args.Slice, "s", false, "slice mode. no message box")
	flag.BoolVar(&args.Verbose, "v", false, "print detailed log")
	flag.BoolVar(&args.Force, "f", false, "force mode")

	flag.BoolVar(&args.Install, "i", false, "install package")
	flag.BoolVar(&args.Remove, "r", false, "remove package(file only)")
	flag.BoolVar(&args.Purge, "P", false, "purge package(file + config)")
	flag.BoolVar(&args.List, "l", false, "list installed script")
	flag.BoolVar(&args.Json, "json", false, "list by json")
	flag.Usage = printUsage
	flag.Parse()

	switch strings.ToLower(flag.Arg(0)) {
	case "version":
		fmt.Println(bipkg.Version)
		return
	}

	// logx level
	bipkg.Logs.OutputFlag = logx.OutputFlag_Console | logx.OutputFlag_File
	bipkg.Logs.OutputLevel = logx.OutputLevel_Info
	if args.Verbose {
		bipkg.Logs.OutputLevel = logx.OutputLevel_Debug
		bipkg.Logs.TimeFlag = logx.Lshortfile | logx.Ldate | logx.Ltime
	}

	// list or install
	scriptFile := flag.Arg(0)
	if args.List == false && args.Install == false && args.Remove == false {
		if _, e := os.Stat("setup.script"); e != nil {
			args.List = true
		} else {
			args.Install = true
			scriptFile = "setup.script"
		}
	}

	if args.List {
		if args.Json {
			bipkg.PrintPkgListJson()
		} else {
			bipkg.PrintPkgList()
		}
		return
	}

	if args.Install {
		if e := bipkg.Install(scriptFile, &bipkg.Args{
			Slice:                   args.Slice,
			IgnoreExecutionError:    args.Force,
			IgnoreInstalledChecking: false,
			UninstallPurge:          false, //ignore
		}); e != nil {
			os.Exit(1)
		}
	}

	if args.Remove {
		if e := bipkg.Uninstall(scriptFile, &bipkg.Args{
			Slice:                   args.Slice,
			IgnoreExecutionError:    args.Force,
			IgnoreInstalledChecking: false,
			UninstallPurge:          false,
		}); e != nil {
			os.Exit(1)
		}
	}

	if args.Purge {
		if e := bipkg.Uninstall(scriptFile, &bipkg.Args{
			Slice:                   args.Slice,
			IgnoreExecutionError:    args.Force,
			IgnoreInstalledChecking: false,
			UninstallPurge:          false,
		}); e != nil {
			os.Exit(1)
		}
	}

	os.Exit(0)
}
