package bipkg

import (
	"fmt"
	"golang.org/x/sys/windows"
	"golang.org/x/sys/windows/registry"
	"strconv"
	"strings"
	"syscall"
)

func init() {

	// path [32key/64key] key type data
	LineFuncMap["reg"] = reg
	LineFuncInfo["reg"] = `reg path [32key/64key] key type data`

	//LineFuncMap["delreg"] = delreg

	// path [32key/64key] value
	//LineFuncMap["delregvalue"] = delregvalue

	//
	LineFuncMap["readreg"] = readreg
	LineFuncInfo["readReg"] = "readReg keyPath [32key/64key] value"

	// autorun name targetPath
	LineFuncMap["autorun"] = autorun
	LineFuncInfo["createAutorun"] = "autorun name runPath"

	// delautorun name
	LineFuncMap["delautorun"] = delautorun
	LineFuncInfo["delAutorun"] = `delautorun name`

	LineFuncInfo["delReqKey"] = `del HKEY_xxx\xxx`
	LineFuncInfo["delReqValue"] = `del HKEY_xxx\xxx [32key/64key] value`
}

func reg(t *ScriptLine) (res string, e error) {
	if t.ArgsN == 4 {
		return "", WriteReg(t.Args[0], ``, t.Args[1], t.Args[2], t.Args[3])
	} else if t.ArgsN >= 5 {
		return "", WriteReg(t.Args[0], t.Args[1], t.Args[2], t.Args[3], t.Args[4])
	}
	return
}
func delreg(t *ScriptLine) (res string, e error) {
	if t.ArgsN >= 1 {
		return "", DeleteReg(t.Args[0])
	}
	return
}
func delregvalue(t *ScriptLine) (res string, e error) {
	if t.ArgsN == 2 {
		return "", DeleteRegValue(t.Args[0], ``, t.Args[1])
	} else if t.ArgsN >= 3 {
		return "", DeleteRegValue(t.Args[0], t.Args[1], t.Args[2])
	}
	return
}
func readreg(t *ScriptLine) (res string, e error) {
	return ReadRegKey(t.arg(0), t.arg(1), t.arg(2))
}
func autorun(t *ScriptLine) (res string, e error) {
	return "", WriteReg(`HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run`, ``,
		t.arg(0), `REG_SZ`, t.arg(1))
}
func delautorun(t *ScriptLine) (res string, e error) {
	return "", DeleteRegValue(`HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run`, ``,
		t.arg(0))
}

// Files
func MoveAndDelete(des string) {
	deletepath := des + ".delete"
	Logs.Infof("MoveAndDelete %v %v", des, deletepath)
	pDes, _ := syscall.UTF16PtrFromString(des)
	pDeletepath, _ := syscall.UTF16PtrFromString(deletepath)
	windows.MoveFileEx(pDes, pDeletepath, windows.MOVEFILE_DELAY_UNTIL_REBOOT)
}

// 注册表
func WriteReg(path, access, key, type_, value string) error {
	Logs.Infof("SetReg %v %x %v %v", path, getRegAccess(access, registry.ALL_ACCESS), key, value)

	rootKey, subPath := parseRegPath(path)

	// Logs.Println("path:", path)
	reg, err := registry.OpenKey(rootKey, subPath, getRegAccess(access, registry.ALL_ACCESS))
	if err != nil {
		reg, _, err = registry.CreateKey(rootKey, subPath, getRegAccess(access, registry.ALL_ACCESS))
		if err != nil {
			Logs.Error(err)
			return err
		}
	}
	defer reg.Close()

	switch type_ {
	case "REG_SZ":
		err = reg.SetStringValue(key, value)
		if err != nil {
			Logs.Error(err)
			return err
		}
	case "REG_EXPAND_SZ":
		err = reg.SetExpandStringValue(key, value)
		if err != nil {
			Logs.Error(err)
			return err
		}
	case "REG_DWORD":
		i, err := strconv.Atoi(value)
		if err != nil {
			Logs.Error(err)
			return err
		}
		err = reg.SetDWordValue(key, uint32(i))
		if err != nil {
			Logs.Error(err)
			return err
		}
		// case "REG_MULTI_SZ":
	default:
		err = fmt.Errorf("no support type (%v)", type_)
		Logs.Error(err)
		return err
	}

	return nil
}

func DeleteReg(path string) error {
	Logs.Infof("DeleteRegKey %v", path)

	rootKey, subPath := parseRegPath(path)

	if err := registry.DeleteKey(rootKey, subPath); err != nil {
		Logs.Error(err)
		return fmt.Errorf("%v %v", err, path)
	}
	return nil
}

func DeleteRegValue(path, access, key string) error {
	Logs.Infof("DeleteReg %v %x %v", path, getRegAccess(access, registry.ALL_ACCESS), key)

	rootKey, subPath := parseRegPath(path)

	reg, err := registry.OpenKey(rootKey, subPath, getRegAccess(access, registry.ALL_ACCESS))
	if err != nil {
		return nil
	}
	defer reg.Close()

	err = reg.DeleteValue(key)
	if err != nil {
		Logs.Error(err)
		return fmt.Errorf("%v %v %v", err, path, key)
	}

	return nil
}
func ReadRegKey(path, access, key string) (value string, e error) {
	rootKey, subPath := parseRegPath(path)

	reg, e := registry.OpenKey(rootKey, subPath, getRegAccess(access, registry.READ))
	if e != nil {
		return "", e
	}
	defer reg.Close()

	_, typ, e := reg.GetValue(key, nil)
	switch typ {
	default:
		return "", registry.ErrUnexpectedType
	case registry.SZ, registry.EXPAND_SZ:
		value, _, e = reg.GetStringValue(key)
		return
	case registry.BINARY:
		v, _, e := reg.GetBinaryValue(key)
		value = string(v)
		return value, e
	case registry.DWORD, registry.QWORD:
		v, _, e := reg.GetIntegerValue(key)
		value = strconv.FormatInt(int64(v), 10)
		return value, e
	}
}

func parseRegPath(path string) (rootKey registry.Key, sub string) {
	if strings.Index(path, "HKEY_CLASSES_ROOT") == 0 {
		rootKey = registry.CLASSES_ROOT
		sub = path[len("HKEY_CLASSES_ROOT")+1:]
	} else if strings.Index(path, "HKEY_CURRENT_USER") == 0 {
		rootKey = registry.CURRENT_USER
		sub = path[len("HKEY_CURRENT_USER")+1:]
	} else if strings.Index(path, "HKEY_LOCAL_MACHINE") == 0 {
		rootKey = registry.LOCAL_MACHINE
		sub = path[len("HKEY_LOCAL_MACHINE")+1:]
	} else if strings.Index(path, "HKEY_USERS") == 0 {
		rootKey = registry.USERS
		sub = path[len("HKEY_USERS")+1:]
	} else if strings.Index(path, "HKEY_CURRENT_CONFIG") == 0 {
		rootKey = registry.CURRENT_CONFIG
		sub = path[len("HKEY_CURRENT_CONFIG")+1:]
	}
	return
}

func getRegAccess(access string, base uint32) uint32 {
	if strings.Contains(access, "32key") {
		base |= registry.WOW64_32KEY
	}
	if strings.Contains(access, "64key") {
		base |= registry.WOW64_64KEY
	}
	return base
}
