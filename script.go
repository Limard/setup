package bipkg

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

// Script
type Script struct {
	FilePath         string
	Name             string   // 名称，用于显示、判断
	Version          string   // 版本，用于显示、判断
	OS 				 string
	Arch    	     string
	Depends          []string // 遍历script中的信息，用于显示、判断
	WorkDir          string   // 工作路径
	InstallSection   ScriptSection
	UninstallSection ScriptSection
	ConfFilesSection ConfFilesSection
	OtherSection     map[string]*ScriptSection
	Env              map[string]string
	buffer           []byte // 解析前的全文
	runArgs          *Args  // 运行参数
}

// parse buffer to Script
func GetScript(f io.Reader, gEnv map[string]string, args *Args) (*Script, error) {
	// if scriptPath is a compress package
	// todo: 支持zip
	//var zipTmp = ""
	//if strings.HasSuffix(f.Name(), ".zip") || strings.HasSuffix(f.Name(), ".bipkg") {
	//	zipTmp, _ = ioutil.TempDir("", "bipkg_")
	//	unzipReader(f, zipTmp)
	//}
	//defer func() {
	//	if zipTmp != "" {
	//		//os.RemoveAll(zipTmp)
	//	}
	//}()

	buf, e := ioutil.ReadAll(f)
	if e != nil {
		return nil, e
	}

	t := &Script{
		buffer:       buf,
		OtherSection: make(map[string]*ScriptSection),
		Env:          gEnv,
		runArgs:      args,
	}
	if t.Env == nil {
		t.Env = make(map[string]string)
	}
	for s, s2 := range getSysPathVariables() {
		t.Env[s] = s2
	}

	if ff, ok := f.(*os.File); ok {
		t.FilePath = ff.Name()
	}

	lineNumber := 1
	sectionName := sectionNameInstall
	rd := bufio.NewReader(bytes.NewReader(buf))
	for {
		lineStr, err := rd.ReadString('\n') //以'\n'为结束符读入一行
		lineNumber++

		if lineStr != "" {
			// section切换
			lineStr := strings.TrimSpace(lineStr)
			if strings.HasPrefix(lineStr, "[") && strings.HasSuffix(lineStr, "]") {
				sectionName = lineStr
				continue
			}

			// conffiles
			if sectionName == sectionNameConfFiles {
				t.ConfFilesSection = append(t.ConfFilesSection, lineStr)
				continue
			}

			// script...
			line := getScriptLine(lineNumber, lineStr)
			if line == nil {
				continue
			}

			if line.Keyword == "set" {
				if len(line.Args) < 2 {
					return nil, fmt.Errorf("the args of 'set' is mistake. src:%v, args:%v", line.Src, line.Args)
				}

				switch strings.ToLower(line.Args[0]) {
				case "name":
					t.Name = line.Args[1]
				case "version":
					t.Version = line.Args[1]
				case "os":
					t.OS = line.Args[1]
				case "arch":
					t.Arch = line.Args[1]
				}
				t.Env[line.Args[0]] = line.Args[1]
				continue
			}

			if line.Keyword == "depend" {
				t.Depends = append(t.Depends, line.Args[0])
			}

			k := strings.TrimSpace(line.Keyword)
			if strings.HasPrefix(k, "[") && strings.HasSuffix(k, "]") {
				sectionName = k
			}

			switch sectionName {
			default:
				s, ok := t.OtherSection[sectionName]
				if ok {
					s.Scripts = append(s.Scripts, line)
				} else {
					s = &ScriptSection{
						sectionName: sectionName,
					}
					s.Scripts = append(s.Scripts, line)
					t.OtherSection[sectionName] = s
				}
			case sectionNameInstall:
				t.InstallSection.Scripts = append(t.InstallSection.Scripts, line)
			case sectionNameUninstall:
				t.UninstallSection.Scripts = append(t.UninstallSection.Scripts, line)
			case sectionNameConfFiles:
				// unexpected
			}
		}

		if err != nil {
			break // end
		}
	}

	t.fillEnv()

	return t, nil
}

func (t *Script) RunInstallSection() (e error) {
	// 20200103修改逻辑：静默情况下改为默认安装，不验证版本
	if t.runArgs.IgnoreInstalledChecking == false {
		isNew, installedVersion, e := t.checkVersion()
		if e != nil {
			Logs.Error(e)
			return e
		}
		if isNew == false {
			if t.runArgs.Slice == false {
				msg := fmt.Sprintf("安装包%v，当前已安装版本%v，是否安装%v？", t.Name, installedVersion, t.Version)
				if messageBoxYesNo(msg, "版本确认") == false {
					return nil
				}
			}
		}
	}

	if _, e := t.runSection(&t.InstallSection); e != nil {
		// 安装失败后尝试执行卸载
		Logs.Error(e)
		Logs.Info("尝试清理已安装部分")
		f := Logs.OutputFlag
		Logs.OutputFlag = 0
		t.runSection(&t.UninstallSection)
		Logs.OutputFlag = f
		return e
	}

	if e := PkgRecord.SavePkg(t.buffer, t.Name); e != nil {
		Logs.Warnf("%v", e)
	}
	return nil
}

func (t *Script) RunUninstallSection() (e error) {
	if _, e = t.runSection(&t.UninstallSection); e != nil {
		Logs.Error(e)
		return e
	}

	if name, ok := t.Env["name"]; ok {
		if e = PkgRecord.DelPkg(name); e != nil {
			Logs.Warnf("%v", e)
			return e
		}
	}
	return nil
}

func (t *Script) RunSection(sectionName string) error {
	sectionName = strings.ToLower(sectionName)

	wd, _ := os.Getwd()
	if len(t.WorkDir) > 0 {
		os.Chdir(t.WorkDir)
	}
	defer os.Chdir(wd)

	var e error
	switch sectionName {
	default:
		section, ok := t.OtherSection[sectionName]
		if ok == false {
			Logs.Warnf("no section %v", sectionName)
			return nil
		}

		_, e = t.runSection(section)
	case sectionNameInstall:
		e = t.RunInstallSection()
	case sectionNameUninstall:
		e = t.RunUninstallSection()
	}
	return e
}

func (t *Script) checkVersion() (isNew bool, installedVersion string, e error) {
	for _, r := range PkgRecord.ListPkg() {
		if r.Name == t.Name {
			n, e := VersionCompare(r.Version, t.Version)
			if e != nil {
				Logs.Error(e)
				return true, r.Version, e
			}
			if n <= 0 {
				return false, r.Version, nil
			}
			return true, r.Version, nil
		}
	}
	return true, "", nil
}

func (t *Script) compareVersion() (isNew bool, installedVersion string, e error) {
	for _, r := range PkgRecord.ListPkg() {
		if r.Name == t.Name {
			n, e := VersionCompare(r.Version, t.Version)
			if e != nil {
				Logs.Error(e)
				return true, r.Version, e
			}
			if n <= 0 {
				return false, r.Version, nil
			}
			return true, r.Version, nil
		}
	}
	return true, "", nil
}

func (t *Script) fillEnv() {
	if _, ok := t.Env["title"]; ok == false {
		t.Env["title"] = t.Env["name"]
	}

	if _, ok := t.Env["startinstall"]; ok == false {
		t.Env["startinstall"] = "开始安装"
	}
	if _, ok := t.Env["installsuccess"]; ok == false {
		t.Env["installsuccess"] = "安装完成"
	}
	if _, ok := t.Env["installfailed"]; ok == false {
		t.Env["installfailed"] = "安装失败，详见日志信息"
	}

	if _, ok := t.Env["startuninstall"]; ok == false {
		t.Env["startuninstall"] = "开始卸载"
	}
	if _, ok := t.Env["uninstallsuccess"]; ok == false {
		t.Env["uninstallsuccess"] = "卸载完成"
	}
	if _, ok := t.Env["uninstallfailed"]; ok == false {
		t.Env["uninstallfailed"] = "卸载失败，详见日志信息"
	}

	//for key, value := range t.env {
	//	Logs.Debugf("%v=%v", key, value)
	//}
}

// Section
func (t *Script) runSection(section *ScriptSection) (result []LineRunResult, e error) {
	Logs.Info("Name:", t.Env["Name"])
	Logs.Info("Version:", t.Env["Version"])
	Logs.Info("Section:", section.sectionName)
	for _, line := range section.Scripts {
		r := LineRunResult{}
		r.Result, e = t.runLine(line)
		if e != nil {
			r.Error = e.Error()
		}
		result = append(result, r)
		if e != nil && !t.runArgs.IgnoreExecutionError {
			return
		}
	}
	e = nil
	return
}

// Line
func (t *Script) runLine(line *ScriptLine) (result string, e error) {
	line.parseVariable(t.Env)

	switch line.Keyword {
	default:
		ParsePaths(line.Args, t.Env)

		if f, ok := LineFuncMap[line.Keyword]; ok {
			result, e = f(line)
			if e != nil {
				//Logs.Warnf("line:%d %s (%s)", line.LineNumber, line.Src, e.Error())
			}
		} else {
			c := []string{line.Keyword}
			c = append(c, line.Args[0:]...)
			e = Command(c, true)
			if e != nil {
				//Logs.Warnf("line:%d %s (%s)", line.LineNumber, line.Src, e.Error())
			}
		}
	case "call":
		e = CallScriptFile(line.arg(0), line.arg(1), nil)
	case "section":
		Logs.Debugf(" === run section %s ===", line.arg(0))
		defer Logs.Debugf(" === run section %s end ===", line.arg(0))
		e = t.RunSection(line.arg(0))

	case "placeholderjson":
		_, e = placeholderJson(line, t.Env)
	case "copy":
		_, e = copyFile(line, t.ConfFilesSection)
	case "del":
		if t.runArgs.UninstallPurge {
			_, e = del(line, nil)
		} else {
			_, e = del(line, t.ConfFilesSection)
		}
	}
	if e != nil {
		if t.runArgs.IgnoreExecutionError || line.ignoreExecutionError {
			Logs.Warnf("line:%d %s (%s)", line.LineNumber, line.Src, e.Error())
			e = nil
		} else {
			Logs.Errorf("line:%d %s (%s)", line.LineNumber, line.Src, e.Error())
		}
	}
	return
}

func init() {
	//LineFuncInfo["copy"] = ""
}

// ScriptSection
type ScriptSection struct {
	sectionName string
	Scripts     []*ScriptLine
}

type LineRunResult struct {
	Result string
	Error  string
}
