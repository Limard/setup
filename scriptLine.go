package bipkg

import (
	"strings"
)

type ScriptLine struct {
	LineNumber int    // 行号
	Src        string // 解析的原文

	// 以下参数从src中解析获得
	Keyword              string // set, dir...
	Args                 []string
	ArgsN                int
	ignoreExecutionError bool // options: ignoreExecutionError
	nowait               bool // options: nowait
}

// parse a string to scriptLine
func getScriptLine(number int, src string) *ScriptLine {
	src = strings.TrimLeft(src, string([]byte{0xEF, 0xBB, 0xBF}))

	if strings.HasPrefix(src, "//") ||
		strings.HasPrefix(src, "::") ||
		strings.HasPrefix(src, "#") {
		return nil
	}

	src = strings.Trim(src, "\n")
	src = strings.Trim(src, "\r")
	src = strings.TrimSpace(src)
	if src == "" {
		return nil
	}

	cs := ParseCommand(src)

	sl := &ScriptLine{
		LineNumber: number,
		Src:        src,
		Keyword:    strings.ToLower(cs[0]),
	}

	for i := 1; i < len(cs); i++ {
		switch cs[i] {
		case "-x86":
			if Is64OS {
				return nil
			}
		case "-x64":
			if !Is64OS {
				return nil
			}
		case "-nowait":
			sl.nowait = true
		case "-allowfailure", "-f":
			sl.ignoreExecutionError = true
		default:
			sl.Args = cs[i:]
			sl.ArgsN = len(sl.Args)
			return sl
		}
	}
	return sl
}

func (t *ScriptLine) parseVariable(env map[string]string) {
	for i := 0; i < len(t.Args); i++ {
		for key, value := range env {
			t.Args[i] = strings.Replace(t.Args[i], "["+key+"]", value, 1)
		}
	}
}

func (t *ScriptLine) arg(n int) string {
	if len(t.Args) > n {
		return t.Args[n]
	}
	return ""
}