package bipkg

import (
	"github.com/Limard/windowsapi"
	"golang.org/x/sys/windows/registry"
)

func regWritePca() error {
	// refer: http://blog.csdn.net/sx5486510/article/details/48003587
	key, e := registry.OpenKey(registry.CURRENT_USER,
		"Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Compatibility Assistant\\Persisted",
		registry.READ|registry.WRITE)
	if e != nil {
		return e
	}
	defer key.Close()

	apppath := windowsapi.GetAppExePath()
	return key.SetDWordValue(apppath, 1)
}
