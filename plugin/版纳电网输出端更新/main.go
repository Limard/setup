package main

import "C"
import (
	"bitbucket.org/Limard/localmem"
	"fmt"
	"github.com/google/uuid"
)

//export RegisterCmd
func RegisterCmd() []string {
	return []string{
		"BNDLCreateOutputGUID",
	}
}

//export BNDWCreateOutputGUID
func BNDLCreateOutputGUID() error {
	local := &struct {
		ID string `json:"id"`
	}{}
	localmem.GetValue2(`localname`, &local)
	fmt.Printf("%+v\n", local)

	if len(local.ID) == 0 {
		id := uuid.New()
		local.ID = id.String()
		return localmem.SetValueJson(`localname`, local, true)
	}
	return nil
}

func main() {
	BNDLCreateOutputGUID()
}
