package bipkg

import (
	"github.com/kardianos/service"
)

func init() {
	LineFuncMap["sc"] = sc

	LineFuncInfo["scInstall"] =`sc install serviceName exePath [args] [**depends xxx]`
	LineFuncInfo["scDelete"] =`sc delete serviceName`
	LineFuncInfo["scStart"] =`sc start serviceName`
	LineFuncInfo["scStop"] =`sc stop serviceName`
	LineFuncInfo["scRestart"] =`sc restart serviceName`
}

func sc(t *ScriptLine) (res string, e error) {
	switch t.Args[0] {
	case "create":
		// name exepath args... **depends xxxx xxxx
		return "", ServiceInstall(t.Args[1], t.Args[2], t.Args[3:])
	case "delete":
		return "", ServiceUninstall(t.Args[1])
	case "start":
		return "", ServiceStart(t.Args[1])
	case "stop":
		return "", ServiceStop(t.Args[1])
	case "restart":
		return "", ServiceRestart(t.Args[1])
	}
	return
}

func ServiceStart(name string) error {
	Logs.Infof("ServiceStart %v", name)

	config := &service.Config{
		Name: name,
	}

	s, err := service.New(nil, config)
	if err != nil {
		return err
	}

	return s.Start()
}

func ServiceStop(name string) error {
	Logs.Infof("ServiceStop %v", name)

	config := &service.Config{
		Name: name,
	}

	s, err := service.New(nil, config)
	if err != nil {
		return err
	}

	return s.Stop()
}

func ServiceRestart(name string) error {
	Logs.Infof("ServiceRestart %v", name)

	config := &service.Config{
		Name: name,
	}

	s, err := service.New(nil, config)
	if err != nil {
		return err
	}

	return s.Restart()
}

func parseServiceArgs(args []string) ([]string, []string) {
	for i, value := range args {
		if value == "**depends" {
			return args[:i], args[i+1:]
		}
	}
	return args, nil
}

func ServiceInstall(name, executable string, args []string) error {
	args, depends := parseServiceArgs(args)
	Logs.Infof("ServiceInstall %v [executable] %v [args] %v [depends] %v", name, executable, args, depends)

	config := &service.Config{
		Name:         name,
		Executable:   executable,
		Arguments:    args,
		Dependencies: depends,
	}

	s, err := service.New(nil, config)
	if err != nil {
		return err
	}

	return s.Install()
}

func ServiceUninstall(name string) error {
	Logs.Infof("ServiceUninstall %v", name)

	config := &service.Config{
		Name: name,
	}

	s, err := service.New(nil, config)
	if err != nil {
		return err
	}

	s.Stop()

	err = s.Uninstall()
	if err != nil {
		return err
	}

	return serviceUninstallClear(name)
}
