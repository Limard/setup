package bipkg

import (
	"encoding/json"
	"fmt"
	"github.com/Limard/windowsapi"
	"testing"
)

func TestMergeJson(t *testing.T) {
	buf1 := []byte(`{"a":1,"b":2, "c": {"c1": 3, "c2": 4, "c3": ["111", "222"]}}`)
	//buf2 := []byte(`{"d":4,"a":"aaa", "c": {"c2": 5, "c3": 6}}`)
	buf2 := []byte(`{"d":4,"a":"aaa", "c": "ccc"}`)

	var m1, m2 map[string]interface{}

	json.Unmarshal(buf1, &m1)
	json.Unmarshal(buf2, &m2)

	merged := jsonMerge(m1, m2)
	buf, _ := json.Marshal(merged)
	fmt.Println(string(buf))
}

func TestParseCommand(t *testing.T) {
	//for _, v := range windowsapi.ParseCommand(`"C:\123" "D:\123 --333"`) {
	//	t.Log(v)
	//}
	//
	//for _, v := range windowsapi.ParseCommand(`"C:\123" '"D:\123" --333'`) {
	//	t.Log(v)
	//}

	for _, v :=range windowsapi.ParseCommand(`"[ProgramFilesDir]\bis-soft\bisclient\gui\lol-client-main.exe" -config "[ProgramFilesDir]\bis-soft\bisclient\config.json" -mode client`) {
		t.Log(v)
	}
}
