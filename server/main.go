package main

import (
	"bitbucket.org/Limard/logx"
	"github.com/Limard/rpcHttp"
	"github.com/Limard/rpcHttp/jsonrpc2"
	"log"
	"net/http"
	"time"
)

func main() {
	log.Println(`Default data path: .\data\`)
	log.Println(`Default script path: .\data\xxx\setup.script`)

	dlStaticHandler := http.StripPrefix("/data/", http.FileServer(http.Dir("data")))
	http.Handle("/update/", dlStaticHandler)

	sJsonRpc := rpcHttp.NewServer()
	sJsonRpc.RegisterCodec(jsonrpc2.NewCodec(), "application/json")
	sJsonRpc.RegisterCodec(jsonrpc2.NewCodec(), "application/x-www-form-urlencoded")

	//sJsonRpc.RegisterService(new(RFDaemon.PrinterInstallRPC), "")

	mux := http.NewServeMux()
	mux.Handle("/jsonrpc", sJsonRpc)

	server := http.Server{
		Addr:    ":20301",
		Handler: mux,
	}
	err := server.ListenAndServe()
	if err != nil {
		log.Errorf(`%v`, err)
		time.Sleep(1 * time.Second)
	}
}
