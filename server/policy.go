package main

import (
	"fmt"
	"net"
)

//

type Policies struct {
	Pkg struct {
		Pkg      string // MonitorSetup
		Policies []Policy
	}
}

type Policy struct {
	IP       string // 192.168.1.1-192.168.1.255
	Version  int    // 0, 1, 2
	IPKGPath string // MonitorSetup.v1.ipkg
}

func inIP(start, end, check string) error {
	sip := net.ParseIP(start)
	if sip == nil {
		return fmt.Errorf("ParseIP start ip failed")
	}
	eip := net.ParseIP(end)
	if eip == nil {
		return fmt.Errorf("ParseIP end ip failed")
	}
	cip := net.ParseIP(check)
	if cip == nil {
		return fmt.Errorf("ParseIP check ip failed")
	}

	for i := 12; i < 16; i++ {
		if sip[i] > cip[i] || cip[i] > eip[i] {
			return fmt.Errorf("%v failed", i)
		}
	}
	return nil
}
