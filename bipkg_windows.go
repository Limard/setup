package bipkg

import (
	"fmt"
	"github.com/Limard/windowsapi"
	"os"
)

func renewBipkg() error {
	// clear system32
	systemDir, e := windowsapi.GetSystemDir()
	os.Remove(fmt.Sprintf(`%v\bipkg.exe`, systemDir))

	// copy
	currentPath := windowsapi.GetAppExePath()
	winDir, e := windowsapi.GetWindowsDir()
	if e != nil {
		return e
	}
	savePath := fmt.Sprintf(`%v\bipkg.exe`, winDir)

	saveInfo, e := os.Stat(savePath)
	if e != nil && os.IsNotExist(e) {
		return CopyFile(currentPath, savePath)
	}

	currentInfo, e := os.Stat(currentPath)
	if currentInfo.ModTime().After(saveInfo.ModTime()) {
		return CopyFile(currentPath, savePath)
	}

	return nil
}