package bipkg

import (
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
)

func CheckDir() bool {

	return true
}

func CheckFile() bool {

	return true
}

func MakeMD5(filePath string) string {
	buffer, err := ioutil.ReadFile(filePath)
	if err != nil {
		return ""
	}
	md := md5.Sum(buffer)
	return hex.EncodeToString(md[:])
}
