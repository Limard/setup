package bipkg

import (
	"bufio"
	"io"
	"os"
	"path/filepath"
	"testing"
)

func TestParsePath(t *testing.T) {
	t.Log(filepath.IsAbs("abd"))
}

func TestRead(t *testing.T) {
	f, err := os.Open(`C:\ProgramData\PrintSystem\bipkg\pkg\DebugLogSetting`)
	if err != nil {
		return
	}
	defer f.Close()

	counter := 0
	rd := bufio.NewReader(f)
	for {
		lineStr, err := rd.ReadString('\n')
		t.Logf("%v; %v", lineStr, err)
		if err != nil || io.EOF == err {
			counter++
			if counter > 2 {
				break
			}
		}
	}
}