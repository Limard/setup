package bipkg

import "testing"

func Test_parseServiceArgs1(t *testing.T) {
	args := []string{`c:\123.exe`, `-init`, `d:\eee`, `**depends`, `localmemcache`}
	args, depends := parseServiceArgs(args)
	t.Log(args)
	t.Log(depends)
}

func Test_parseServiceArgs2(t *testing.T) {
	args := []string{`c:\123.exe`, `-init`, `d:\eee`, `**depends`}
	args, depends := parseServiceArgs(args)
	t.Log(args)
	t.Log(depends)
}

func Test_parseServiceArgs3(t *testing.T) {
	args := []string{`c:\123.exe`, `-init`, `d:\eee`}
	args, depends := parseServiceArgs(args)
	t.Log(args)
	t.Log(depends)
}
