package bipkg

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const (
	sectionNameInstall   = `[install]`
	sectionNameUninstall = `[uninstall]`
	sectionNameConfFiles = `[conffiles]`
)

type Args struct {
	Slice                   bool //不弹窗
	IgnoreExecutionError    bool //忽略错误，报错后不停止
	IgnoreInstalledChecking bool //
	UninstallPurge          bool
}

// 指定Script文件或Script名，执行install，指定是否强制安装，静默安装
func Install(scriptName string, args *Args) (e error) {
	scriptFile, e := GetScriptFromFile(scriptName)
	if e != nil {
		Logs.Errorf("%v (args: %v)(scriptName: %v)", e, os.Args, scriptName)
		return e
	}
	defer scriptFile.Close()

	hScript, e := GetScript(scriptFile, nil, args)
	if e != nil {
		Logs.Errorf("%v (args: %v)(scriptName: %v)", e, os.Args, scriptName)
		return e
	}

	if !args.Slice {
		if messageBoxOKCancel(hScript.Env["startinstall"], hScript.Env["title"]) == false {
			return nil
		}
	}

	if e = renewBipkg(); e != nil {
		Logs.Error(e)
	}

	// script file
	if e = hScript.RunInstallSection(); e != nil {
		messageBoxOK(fmt.Sprintf("%v. (%v)", hScript.Env["installfailed"], e),
			hScript.Env["title"])
		return e
	}

	if !args.Slice {
		messageBoxOK(hScript.Env["installsuccess"], hScript.Env["title"])
	}
	return nil
}

// 指定Script文件或Script名，执行uninstall，指定是否强制安装，静默安装
func Uninstall(scriptName string, args *Args) (e error) {
	delRecord := false
	script, e := GetScriptFromFile(scriptName)
	if e != nil {
		delRecord = true
		script, e = GetScriptFromInstalled(scriptName)
		if e != nil {
			Logs.Errorf("%v (args: %v)(scriptName: %v)", e, os.Args, scriptName)
			return e
		}
	}
	defer script.Close()

	task, e := GetScript(script, nil, args)
	if e != nil {
		Logs.Errorf("%v (args: %v)(scriptName: %v)", e, os.Args, scriptName)
		return e
	}
	script.Close()

	if !args.Slice {
		if messageBoxOKCancel(task.Env["startuninstall"], task.Env["title"]) == false {
			return nil
		}
	}

	// script file
	if e = task.RunUninstallSection(); e != nil {
		messageBoxOK(fmt.Sprintf("%v. (%v)", task.Env["uninstallfailed"], e),
			task.Env["title"])
		return e
	}

	if delRecord {
		DeleteScriptFromInstalled(scriptName)
	}

	if !args.Slice {
		messageBoxOK(task.Env["uninstallsuccess"], task.Env["title"])
	}

	return nil
}

// PrintPkgList 在控制台输出已安装的pkg列表
func PrintPkgList() {
	var namelen, verlen int
	for _, value := range PkgRecord.ListPkg() {
		if len(value.Name) > namelen {
			namelen = len(value.Name)
		}
		if len(value.Version) > verlen {
			verlen = len(value.Version)
		}
	}

	format := "%-" + strconv.Itoa(namelen) + "s\t%-" + strconv.Itoa(verlen) + "s\t%s"
	fmt.Println(fmt.Sprintf(format, "PkgName", "Version", "Depends"))
	for _, value := range PkgRecord.ListPkg() {
		fmt.Println(fmt.Sprintf(format, value.Name, value.Version, strings.Join(value.Depends, ",")))
	}
}

func PrintPkgListJson() {
	p := PkgRecord.ListPkg()
	b, _ := json.MarshalIndent(p, "", "  ")
	fmt.Println(string(b))
}

// 执行Script File，指定全局变量，执行的section名，是否逆向执行
func CallScriptFile(scriptPath string, sectionName string, args *Args) error {
	f, e := GetScriptFromFile(scriptPath)
	if e != nil {
		return e
	}
	defer f.Close()

	script, e := GetScript(f, nil, args)
	if e != nil {
		Logs.Errorf("parse script file error. %v. %v", scriptPath, e)
		return e
	}
	return script.RunSection(sectionName)
}

// 从 本地/网络 获取文件
func GetScriptFromFile(script string) (f io.ReadCloser, e error) {
	return GetFile(script)
}

func GetScriptFromInstalled(name string) (f io.ReadCloser, e error) {
	for _, v := range PkgRecord.ListPkg() {
		if v.Name == name {
			return os.Open(v.FilePath)
		}
	}
	return nil, fmt.Errorf("cannot find %s", name)
}

func DeleteScriptFromInstalled(name string) (e error) {
	for _, v := range PkgRecord.ListPkg() {
		if v.Name == name {
			return os.Remove(v.FilePath)
		}
	}
	return nil
}

func GetFile(path string) (r io.ReadCloser, e error) {
	// 网络文件
	if strings.HasPrefix(path, "http://") ||
		strings.HasPrefix(path, "https://") {
		resp, e := http.Get(path)
		if e != nil {
			return nil, e
		}
		return resp.Body, nil
	}

	// 本地文件
	return os.Open(path)
}

func GetFileBuffer(path string) (buf []byte, e error) {
	r, e := GetFile(path)
	if e != nil {
		return nil, e
	}
	defer r.Close()

	return ioutil.ReadAll(r)
}
