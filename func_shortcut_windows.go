package bipkg

import (
	"github.com/Limard/windowsapi"
	"github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
	"os"
	"path/filepath"
	"strings"
)

func init() {
	LineFuncMap["shortcut"] = shortcut
	LineFuncInfo["shortcut"] = `shortcut shortcutPath targetPath [description]`
}

func shortcut(t *ScriptLine) (res string, e error) {
	return "", MakeShortcut(t.arg(0), t.arg(1), t.arg(2))
}

//func delshortcut(t *ScriptLine) (res string, e error) {
//	return "", Delete(t.arg(0))
//}

// WScriptShell create OLE Object for "WScript.Shell"
func WScriptShell() (*ole.IUnknown, *ole.IDispatch, error) {
	agent, err := oleutil.CreateObject("WScript.Shell")
	if err != nil {
		return nil, nil, err
	}
	agentDis, err := agent.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		agent.Release()
		return nil, nil, err
	}
	return agent, agentDis, nil
}

func MakeShortcut(shortcutPath, target, description string) (e error) {
	Logs.Infof("Create shortcut %v <- %v (%v)", shortcutPath, target, description)
	ole.CoInitialize(0)
	defer ole.CoUninitialize()

	agent, agentDis, err := WScriptShell()
	if err != nil {
		return err
	}
	defer agent.Release()
	defer agentDis.Release()

	shortcut, err := oleutil.CallMethod(agentDis, "CreateShortCut", shortcutPath)
	if err != nil {
		return err
	}
	shortcutDis := shortcut.ToIDispatch()
	defer shortcutDis.Release()

	targets := windowsapi.ParseCommand(target)
	_, err = oleutil.PutProperty(shortcutDis, "TargetPath", targets[0])
	if err != nil {
		return err
	}

	var arg string
	if strings.HasPrefix(target, `"`) {
		arg = target[len(targets[0])+2:]
	} else {
		arg = target[len(targets[0]):]
	}
	_, err = oleutil.PutProperty(shortcutDis, "Arguments", arg)
	if err != nil {
		return err
	}
	_, err = oleutil.PutProperty(shortcutDis, "Description", description)
	if err != nil {
		return err
	}
	dir, _ := filepath.Split(targets[0])
	e = os.MkdirAll(dir, 0666)
	if e != nil {
		Logs.Warn(e)
	}
	_, err = oleutil.PutProperty(shortcutDis, "WorkingDirectory", dir)
	if err != nil {
		return err
	}
	_, err = oleutil.CallMethod(shortcutDis, "Save")
	return err
}
