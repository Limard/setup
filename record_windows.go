package bipkg

import (
	"github.com/Limard/windowsapi"
	"path/filepath"
)

func getPkgRecord() *pkgRecord {
	p, _ := windowsapi.GetCommmonAppDataDirectory()
	p = filepath.Join(p, `PrintSystem`, `bipkg`)
	return &pkgRecord{
		WorkDir: p,
	}
}