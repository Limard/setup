package bipkg

import "testing"

func TestVersionCompare(t *testing.T) {
	t.Log(VersionCompare("1.18.705.0", "1.18.705.1"))
	t.Log(VersionCompare("2.18.705.0", "1.18.705.0"))
	t.Log(VersionCompare("1.18.705", "1.18.705.0"))
	t.Log(VersionCompare("1.18.705.0", "1.18"))
	t.Log(VersionCompare("1.18.705.0", "1.19"))
	t.Log(VersionCompare("1.18.705.0", "1.19.z"))
}
