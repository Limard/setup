package bipkg

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

// 所有可处理的指令
var LineFuncMap = make(map[string]func(l *ScriptLine) (string, error))
var LineFuncInfo = make(map[string]string)

func init() {
	LineFuncInfo[`call`] = `call path\xxx.script sectionName`
	LineFuncInfo[`section`] = `section -f [uninstall]`

	LineFuncMap["cmd"] = cmd

	LineFuncMap["sleep"] = sleep
	LineFuncInfo[`sleep`] = `sleep ms`

	LineFuncMap["readpkg"] = readpkg
	LineFuncInfo["readpkg"] = `readpkg`

	LineFuncMap["unpkg"] = unpkg
	LineFuncInfo["unpkg"] = `unpkg`

	//LineFuncMap["bndlcreateoutputguid"] = bndlcreateoutputguid
}

func cmd(t *ScriptLine) (res string, e error) {
	return "", Command(t.Args[0:], !t.nowait)
}
func sleep(t *ScriptLine) (res string, e error) {
	tm, _ := strconv.Atoi(t.arg(0))
	time.Sleep(time.Duration(tm) * time.Millisecond)
	return "", nil
}
//读取已安装的pkg的script
func readpkg(t *ScriptLine) (res string, e error) {
	nm := t.arg(0)
	if len(nm) > 0 {
		for _, v := range PkgRecord.ListPkg() {
			if v.Name == nm {
				b, _ := json.Marshal(v)
				return string(b), nil
			}
		}
		return "", fmt.Errorf("cannot find package %s", nm)
	}
	return
}
//卸载已安装的pkg
func unpkg(t *ScriptLine) (res string, e error) {
	nm := t.arg(0)
	if len(nm) > 0 {
		for _, v := range PkgRecord.ListPkg() {
			if v.Name == nm {
				return "", v.RunUninstallSection()
			}
		}
		return "", fmt.Errorf("cannot find package %s", nm)
	}
	return
}

// BNDLCreateOutputGUID 临时：云南版纳电网
//func bndlcreateoutputguid(t *ScriptLine) (res string, e error) {
//	local := &struct {
//		ID string `json:"id"`
//	}{}
//	localmem.GetValue2(`localname`, &local)
//	if len(local.ID) == 0 {
//		id := uuid.New()
//		local.ID = id.String()
//		return "", localmem.SetValueJson(`localname`, local, true)
//	}
//	return "", nil
//}

func PrintFunc() {
	for _, s := range LineFuncInfo {
		fmt.Println(s)
	}
}
