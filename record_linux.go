package bipkg

import (
	"os"
	"syscall"
)

func getPkgRecord() *pkgRecord {
	s := `/var/lib/bipkg`

	syscall.Umask(0)
	os.MkdirAll(s, 0666)
	return &pkgRecord{
		WorkDir: s,
	}
}