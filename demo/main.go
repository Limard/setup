package main

import (
	"bitbucket.org/Limard/bipkg"
	"fmt"
)

func main() {
	for key := range bipkg.LineFuncMap {
		fmt.Printf("%s\n", key)
	}
}
