# bipkg使用说明

## 启动参数
-s 静默启动，不弹出MessageBox
-v 输出详细日志信息
-l 显示当前安装的PKG列表
-i 执行安装脚本（默认）
-u 执行卸载脚本

## setup.script命令

### 固定信息

```
set Name "PrintMonitor"
set Version "1.0.18.503"
set Title "安装程序"
set StartInstall "开始安装.....程序"
set InstallSuccess "安装完成"
set InstallFailed "安装失败"
```

### 命令行基本格式

command  [arguments]

**command**不区分大小写

**options**

-x86：仅在x86系统执行
-x64：仅在x86系统执行
-f：该命令执行失败，不停止安装进程
-nowait：cmd用，不等待cmd执行完成，继续执行后续命令

**arguments**详见各command

### 文件/文件夹

```
file desPath [srcPath [notexist]]
	创建/复制文件
	[srcPath]：如果不存在，则仅创建文件。路径可为本地文件，或http，https的远端文件
	[notexist]：仅当desPath不存在时，才复制文件。用于配置文件

dir desPath [srcPath]
	创建/复制文件夹
	[srcPath]：如果不存在，则仅创建文件。路径可为本地文件，如配合FileTransfer，可为http,https的远端文件夹
	
del desPath
	删除指定文件/文件夹

rename desPath srcPath
	重命名/移动文件/文件夹
	
readfile desPath [rangs]
	读取指定文件
	[rangs]：读取的范围，格式为 0-1024（包含0，不包含1024），如果[rangs]不存在，则默认读取0-4096
```

### 注册表操作

```
写入注册表
	reg keyPath [32key/64key] value type data
		[32key/64key]：64位系统时，写入WOW64路径下或64bit路径下
	
删除指定注册表	
	del keyPath

删除指定注册表键值
	del keyPath [32key/64key] value
	
读取指定注册表键值		
	readReg keyPath [32key/64key] value
```

### 开机自动启动

```
autorun name runPath
	创建开机自动启动（用注册表实现）
	
delAutorun name
	删除开机自动启动（用注册表实现）
```

### 快捷方式

```
shortcut shortcutPath targetPath [description]
	创建快捷方式
	
delShortcut shortcutPath
	删除快捷方式（功能同delfile）
```

### LocalmemFile配置文件

```
localmemimport name filepath [cipher]
	将指定文件导入到localmemFile中
	[cipher]：是否加密，默认false
	
localmemremove name`
	删除指定localmem项目
	
localmemmerge name filepath [cipher]
	将指定文件合并到localmemFile中
	[cipher]：是否加密，默认false
	
readlocalmem name
	读取指定文件的值
```

**cipher为该配置文件是否需要加密保存，C++程序使用的LocamMemFile必须加密。**

### 系统服务

```
sc install serviceName exePath [args] [**depends xxx]
	注册系统服务
	[**depends xxx]: 为关键词
	
sc delete serviceName
	删除系统服务
	
sc start serviceName
	开始系统服务
	
sc stop serviceName
	停止系统服务
	
sc restart serviceName
	重启系统服务
```

### WOW64

```
wow64 disable
	临时禁用WOW64
	
wow64 enable
	恢复WOW64
```

### setup安装程序兼容设定

```
moduleInfo moduleName moduleVersion
	兼容老系统，将模块名和版本，写入列表
	
delModuleInfo moduleName
	兼容老系统，删除模块
```

### 依赖检查

```
depend item1 [call xxxx]
    用于检查依赖项目
    可选值为：[vc80x86]、[vc80x64]、[vc90x86]、[vc90x64]、[vc100x86]、[vc100x64]，和由bipkg或本系统安装的PkgName
    [call xxxx]：不存在时尝试安装的script
```

### 调用其他脚本

```
call xxxxxx.script
	执行指定的script脚本

section [xxxxx]
    调用其他section
```

### 命令行调用


```
cmd [options] command
	执行系统的command命令
```

### 暂停

```
sleep [options] ms
	暂定等待
```

### 读取Pkg信息

```
readpkg pkgName
	读取指定名称的package，返回值为json字符串
```

## Windows系统路径

```
[CommonDesktopDir]
[PrinterDriverDirX86]
[PrinterDriverDirX64]
[PrinterProcessorDirX86]
[PrinterProcessorDirX64]
[WindowsDir]
[ProgramFilesDir]
[ProgramFiles86Dir]
[CommonAppDataDir]
[SystemDir]
[SystemDir86]
[TempDir]
[StartMenuDir]
[Local] （bipkg所在路径）
[CommonStartUp]
[StartUp]
```

## 调用例

```
set Name "PrintMonitor"
set Version "1.0.18.503"

[install]
section -f [uninstall]

depend [vc80x86] call xxxxx

file desPath [srcPath]
dir desPath [srcPath]
reg key value type data

autorun NAME RUNPATH
shortcut SHORTCUTPATH TARGETPATH [DESCRIPTION]

cmd [-nowait] COMMAND

localmemimportname FILEPATH [cipher(false)]
localmemmerge name FILEPATH [cipher(false)]

sc create NAME EXEPATH [ARGS] [**depends DEPENDS]
sc delete NAME
sc start NAME
sc stop NAME
sc restart NAME


[uninstall]
delfile desPath
deldir desPath

delReg key
delRegValue key value

delAutorun name

delShortcut shortcutPath

localmemremove name
```

