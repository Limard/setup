package bipkg

import (
	"bytes"
	"os"
	"path/filepath"
	"time"
)

// [CommonAppDataDir]\PrintSystem\bipkg\bin
// [CommonAppDataDir]\PrintSystem\bipkg\pkg

var PkgRecord = getPkgRecord()

type pkgRecord struct {
	WorkDir string // [CommonAppDataDir]\PrintSystem\bipkg\
}

// 保存setup.script到备份区，用于枚举和卸载
func (t *pkgRecord) SavePkg(buffer []byte, name string) error {
	return CopyFileFromBuffer(bytes.NewReader(buffer), filepath.Join(t.WorkDir, `pkg`, name), time.Now())
}

func (t *pkgRecord) DelPkg(name string) error {
	pkgPath := filepath.Join(t.WorkDir, `pkg`, name)
	return os.Remove(pkgPath)
}

func (t *pkgRecord) ListPkg() (list []*Script) {
	pkgPath := filepath.Join(t.WorkDir, `pkg`)
	filepath.Walk(pkgPath, func(fPath string, fInfo os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if fInfo.IsDir() {
			return nil
		}

		f, e := os.Open(fPath)
		if e != nil {
			return nil
		}
		defer f.Close()

		sf, e := GetScript(f, nil, &Args{})
		if e != nil {
			return nil
		}
		list = append(list, sf)
		return nil
	})
	return
}

func (t *pkgRecord) GetPkgPath(name string) string {
	pkgPath := filepath.Join(t.WorkDir, `pkg`, name)

	_, e := os.Stat(pkgPath)
	if e != nil && os.IsNotExist(e) {
		return ""
	}
	return pkgPath
}
