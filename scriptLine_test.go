package bipkg

import (
	"testing"
)

func Test_getLineScript(t *testing.T) {
	// vcredist_x86_vc100.exe /q:a /c:"msiexec /i vcredist.msi /qn /l*v C:\vcredist_x86_vc100.log"
	l := getScriptLine(0,`cmd vcredist_x86_vc100.exe /q:a /c:"msiexec /i vcredist.msi /qn /l*v C:\vcredist_x86_vc100.log"`)
	t.Log(l.Keyword)
	for key, value := range l.Args {
		t.Log(key, value)
	}
}

func TestPrintLineFuncMap(t *testing.T) {
	PrintFunc()
}
