package bipkg

import (
	"path/filepath"
	"testing"
)

func TestParseScript(t *testing.T) {
	//sf, e := GetScript(`D:\GOPATH\src\bitbucket.org\Limard\setup\alone\setup.script`, nil)
	//if e != nil {
	//	t.Fatal(e)
	//}
	////t.Log(sf)
	//for key, value := range sf.ScriptSection {
	//	for _, s := range value {
	//		t.Logf("%v: %v %v", key, s.keyword, s.args)
	//	}
	//}
	//for key, value := range sf.Env {
	//	t.Logf("env: %v = %v", key, value)
	//}
}

func TestName(t *testing.T) {
	t.Log(filepath.Dir(`E:\sdfs\dsfs\dsd'`))
}

func TestArr(t *testing.T) {
	var a []string
	//a = append(a, "aa", "bb" ,"cc", "dd")
	a = append(a, "notepad")
	t.Log(a[1:])
}

func TestMap(t *testing.T) {
	kv := make(map[string]string)
	APPENDKV(kv)
	t.Log(kv)
}

func APPENDKV(kv map[string]string) {
	kv["aa"] = "bb"
	kv["aacc"] = "bbcc"
}

func TestExt(t *testing.T) {
	t.Log(filepath.Ext(`E:\abc\dec.tes`))
}

func TestReadMainScript(t *testing.T) {
	//globalVariable := getMainScriptEnv()
	//t.Log(globalVariable)
}

func TestPrintf(t *testing.T) {
	t.Logf("%6s", "123456")
}
