package bipkg

import "testing"

func Test_placeholderText(t *testing.T) {
	_, e := placeholderJson(&ScriptLine{
		Args:  []string{`D:\temp\200507\config_templ.json`, `D:\temp\200507\config.json`},
		ArgsN: 2,
	}, nil)
	if e != nil {
		t.Fatal(e)
	}
}