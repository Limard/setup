module bitbucket.org/Limard/bipkg

go 1.13

require (
	bitbucket.org/Limard/localmem v0.0.0-20200115055109-c9638669f0da
	bitbucket.org/Limard/logx v0.1.4
	github.com/Limard/rpcHttp v1.2.0
	github.com/Limard/windowsapi v0.0.0-20180920014842-7fdfb544de08
	github.com/go-ole/go-ole v1.2.4
	github.com/google/uuid v1.1.1
	github.com/gorilla/rpc v1.2.0 // indirect
	github.com/kardianos/service v1.0.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/sys v0.0.0-20200511232937-7e40ca221e25
	gopkg.in/ini.v1 v1.56.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
