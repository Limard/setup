package bipkg

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"strconv"
)

//var constValue = make(map[string]string, 15)
var Is64OS bool

func init() {
	LineFuncInfo["copy"] = "copy desPath srcPath [notexist]"
	LineFuncInfo["del file"] = `del c:\xxxxx`

	LineFuncMap["rename"] = rename
	LineFuncInfo["rename"] = "rename desPath srcPath"

	//LineFuncMap["file"] = file
	//LineFuncMap["dir"] = dir
	//LineFuncMap["delfile"] = del
	//LineFuncMap["deldir"] = del

	LineFuncMap["readfile"] = readfile
	LineFuncInfo["readfile"] = "readfile desPath [rangs]: rangs: 0-1024"
}

func copyFile(t *ScriptLine, confFiles ConfFilesSection) (res string, e error) {
	if t.ArgsN == 3 {
		if t.Args[2] == "notexist" {
			if _, e = os.Stat(t.Args[0]); e != nil && os.IsNotExist(e) {
				return "", Copy(t.Args[1], t.Args[0], confFiles)
			}
		}
	}
	if t.ArgsN == 2 {
		return "", Copy(t.Args[1], t.Args[0], confFiles)
	}
	if t.ArgsN == 1 {
		return "", CreateFile(t.Args[0])
	}
	return
}
func del(t *ScriptLine, confFiles ConfFilesSection) (res string, e error) {
	if t.ArgsN == 1 {
		if strings.HasPrefix(t.Args[0], "HKEY_") {
			return "", DeleteReg(t.Args[0])
		} else {
			return "", Delete(t.Args[0], confFiles)
		}
	}
	if t.ArgsN == 2 {
		if strings.HasPrefix(t.Args[0], "HKEY_") {
			return "", DeleteRegValue(t.Args[0], ``, t.Args[1])
		}
	}
	if t.ArgsN == 3 {
		if strings.HasPrefix(t.Args[0], "HKEY_") {
			return "", DeleteRegValue(t.Args[0], t.Args[1], t.Args[2])
		}
	}
	Logs.Warn("cannot support:", t.Src)
	return
}
func rename(t *ScriptLine) (res string, e error) {
	return "", os.Rename(t.arg(1), t.arg(0))
}
func readfile(t *ScriptLine) (res string, e error) {
	return ReadFile(t.arg(0), t.arg(1))
}

func ParsePath(path string, env map[string]string) string {
	for k, v := range env {
		path = strings.Replace(path, k, v, -1)
	}
	return path
}

func ParsePaths(paths []string, env map[string]string) []string {
	for i := 0; i < len(paths); i++ {
		paths[i] = ParsePath(paths[i], env)
	}
	return paths
}

// 文件夹
func CreateDir(path string) error {
	Logs.Infof("%v", path)

	ov := Wow64DisableWow64FsRedirection()
	defer Wow64RevertWow64FsRedirection(ov)

	err := os.MkdirAll(path, 0666)
	if err != nil {
		return fmt.Errorf("%v %v", err, path)
	}
	return nil
}

func Copy(src, des string, confFiles ConfFilesSection) error {
	return filepath.Walk(src, func(fPath string, fInfo os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		absPath := strings.Replace(fPath, src, "", 1)
		desPath := filepath.Join(des, absPath)

		if fInfo.IsDir() {
			return CreateDir(desPath)
		}

		// 如果文件存在，则忽略conf files的文件
		if _, e1 := os.Stat(desPath); e1 == nil || os.IsExist(e1) {
			ignore := false
			for _, s := range confFiles {
				if s == desPath {
					ignore = true
					break
				}
			}
			if ignore {
				return nil
			}
		}

		return CopyFile(fPath, desPath)
	})
}

// 下载文件并保存到指定目录
func CreateFile(des string) error {
	return ioutil.WriteFile(des, []byte{}, 0666)
}

func CopyFile(src, des string) error {
	Logs.Infof("%s <- %s", des, src)

	if strings.HasPrefix(src, "http://") ||
		strings.HasPrefix(src, "https://") {
		return CopyFileFromUrl(src, des)
	}

	f, e := os.Open(src)
	if e != nil {
		Logs.Error(e)
		return e
	}
	defer f.Close()

	modTime := time.Now()
	info, _ := os.Stat(src)
	if info != nil {
		modTime = info.ModTime()
	}
	return CopyFileFromBuffer(f, des, modTime)
}

func CopyFileFromUrl(url, des string) error {
	url = strings.Replace(url, `\`, `/`, -1)
	resp, err := http.Get(url)
	if err != nil {
		Logs.Error(err)
		return err
	}
	defer resp.Body.Close()

	//Logs.Println("CopyFile", url, des)
	return CopyFileFromBuffer(resp.Body, des, time.Now())
}

func CopyFileFromBuffer(reader io.Reader, des string, modTime time.Time) error {
	ov := Wow64DisableWow64FsRedirection()
	defer Wow64RevertWow64FsRedirection(ov)

	if _, err := os.Stat(des); err == nil || os.IsExist(err) {
		Logs.Infof("TryDelete %v", des)
		if err := os.RemoveAll(des); err != nil {
			Logs.Infof("RemoveAll failed %v", err)
			if err = os.Rename(des, des+".del"); err != nil {
				Logs.Infof("Rename failed %v", err)
			}
		}
	}

	os.MkdirAll(filepath.Dir(des), 0777)
	f, err := os.OpenFile(des, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	_, e := io.Copy(f, reader)
	f.Close()
	if e != nil {
		return e
	}

	if err := os.Chtimes(des, modTime, modTime); err != nil {
		Logs.Error(err)
	}
	return nil
}

func Delete(path string, confFiles ConfFilesSection) error {
	Logs.Infof("%v", path)

	ov := Wow64DisableWow64FsRedirection()
	defer Wow64RevertWow64FsRedirection(ov)

	e := filepath.Walk(path, func(fp string, fi os.FileInfo, e error) error {
		if e != nil {
			return e
		}

		ignore := false
		for _, file := range confFiles {
			if strings.HasPrefix(fp, file) {
				ignore = true
				break
			}
		}
		if ignore {
			Logs.Info("ConfFiles:", fp)
			return nil
		}

		Logs.Info(fp)
		e = os.Remove(fp)
		if e != nil {
			Logs.Warn(e)
		}
		return nil
	})
	if e != nil {
		Logs.Error(e)
		return fmt.Errorf("%v %v", e, path)
	}

	// delete dir(忽略错误)
	filepath.Walk(path, func(fp string, fi os.FileInfo, e error) error {
		if e != nil {
			return e
		}
		if fi.IsDir() {
			Logs.Info(fp)
			os.Remove(fp)
		}
		return nil
	})
	return nil
}

func ReadFile(path string, rrange string) (string, error) {
	f, e := os.Open(path)
	if e != nil {
		return "", e
	}
	defer f.Close()

	start := 0
	length := 4096
	r := strings.Split(rrange, "-")
	if len(r) >= 2 {
		start, _ = strconv.Atoi(r[0])
		end, _ := strconv.Atoi(r[1])
		length = end - start
		if length < 0 {
			length = 4096
		}
	}
	buf := make([]byte, length)
	n, e := f.Read(buf)
	return string(buf[:n]), e
}

// 命令行
func Command(cmdstr []string, wait bool) error {
	for i := 0; i < len(cmdstr); i++ {
		cmdstr[i] = strings.Trim(cmdstr[i], "\"")
	}
	cmd := exec.Command(cmdstr[0], cmdstr[1:]...)

	Logs.Info(cmd.Args)

	ov := Wow64DisableWow64FsRedirection()
	defer Wow64RevertWow64FsRedirection(ov)

	if wait {
		ot, e := cmd.Output()
		Logs.Debug(string(ot))
		if e != nil {
			return e
		}
	} else {
		if e := cmd.Start(); e != nil {
			return e
		}
	}
	return nil
}
