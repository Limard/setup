package bipkg

import "testing"

func TestCheckVCRuntime(t *testing.T) {
	// 计算机\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{B0037450-526D-3448-A370-CACBD87769A0}
	t.Log(ParseDepends([]string{"{B0037450-526D-3448-A370-CACBD87769A0}"}).Check())

	// 计算机\HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{13A4EE12-23EA-3371-91EE-EFB36DDFFF3E}
	t.Log(ParseDepends([]string{"{13A4EE12-23EA-3371-91EE-EFB36DDFFF3E}"}).Check())
}

func Test_checkVersion(t *testing.T) {
	d := depends{}
	d.version = ">1.0.0.0"
	if d.checkVersion("1.2.21.3") != true {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != false {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != false {
		t.Fatal("error")
	}

	d.version = ">=1.0.0.0"
	if d.checkVersion("1.2.21.3") != true {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != true {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != false {
		t.Fatal("error")
	}

	d.version = "1.0.0.0"
	if d.checkVersion("1.2.21.3") != true {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != true {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != false {
		t.Fatal("error")
	}

	d.version = "<1.0.0.0"
	if d.checkVersion("1.2.21.3") != false {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != false {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != true {
		t.Fatal("error")
	}

	d.version = "<=1.0.0.0"
	if d.checkVersion("1.2.21.3") != false {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != true {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != true {
		t.Fatal("error")
	}

	d.version = "=1.0.0.0"
	if d.checkVersion("1.2.21.3") != false {
		t.Fatal("error")
	}
	if d.checkVersion("1.0.0.0") != true {
		t.Fatal("error")
	}
	if d.checkVersion("0.2.21.3") != false {
		t.Fatal("error")
	}
}
