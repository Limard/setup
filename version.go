package bipkg

import (
	"bitbucket.org/Limard/logx"
	"fmt"
	"strconv"
	"strings"
)

var (
	Version = "v0.20.5.11"
	Logs    = logx.New("", "")
)

func init() {
	Logs.TimeFlag = logx.Ldate|logx.Ltime
}

// 比较以点分割的版本号，如1.18.705.0
// 0为相等，负数为verCompare < verBase，正数为verCompare > verBase
func VersionCompare(verBase, verCompare string) (int, error) {
	n1, e := versionStringToInt(verBase)
	if e != nil {
		return 0, fmt.Errorf("无法解析版本号(%v)", e)
	}
	n2, e := versionStringToInt(verCompare)
	if e != nil {
		return 0, fmt.Errorf("无法解析版本号(%v)", e)
	}

	n1l := len(n1)
	n2l := len(n2)
	for i := 0; i < n1l && i < n2l; i++ {
		c := n2[i] - n1[i]
		if c != 0 {
			return c, nil
		}
	}
	return n2l - n1l, nil
}

func versionStringToInt(ver string) ([]int, error) {
	s := strings.Split(ver, ".")
	l := len(s)
	var nn []int
	for i := 0; i < l; i++ {
		n, e := strconv.Atoi(s[i])
		if e != nil {
			return nil, e
		}
		nn = append(nn, n)
	}
	return nn, nil
}
